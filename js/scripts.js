(function($) {
    "use strict";

    $('body').scrollspy({
        target: '.navbar-fixed-top',
        offset: 60
    });

    $('#topNav').affix({
        offset: {
            top: 200
        }
    });
    
    new WOW().init();
    
    // $('a.page-scroll').bind('click', function(event) {
    //     var $ele = $(this);
    //     $('html, body').stop().animate({
    //         scrollTop: ($($ele.attr('href')).offset().top - 60)
    //     }, 1450, 'easeInOutExpo');
    //     event.preventDefault();
    // });
    
    $('.navbar-collapse ul li a').click(function() {
        /* always close responsive nav after click */
        $('.navbar-toggle:visible').click();
    });

    $('#galleryModal').on('show.bs.modal', function (e) {
       $('#galleryImage').attr("src",$(e.relatedTarget).data("src"));
    });

    $('#video-background').bind('ended', function(){
        $('#explore').click();
    })  

    $('#explore').click(function () {

        // $('#video-background video').bind('ended', function(){
           // $(this).parent().fadeOut();
        // })  
        // var videoFile = 'asset/loopB.mp4';
        // // var videoFile = 'asset/1.mp4';

        // // $('#video-background video source').attr('src', videoFile);
        // // $("#video-background video")[0].load();

           $('#video-background').fadeOut(1500);
           // $('.inner').hide();

        // var video = document.getElementById('video-background');
        // video.src = videoFile;
        // video.load();
        // video.play();

    });

    //code to show/hide code
    $(function () {
      if ($(".drop-article").outerHeight() < $(".drop-article").get(0).scrollHeight) {
           $('.pin').hide();
       } else {
          $('.pin').show();
       }
    })

    $(window).resize(function () {
        if ($(".drop-article").outerHeight() < $(".drop-article").get(0).scrollHeight) {
            $('.pin').hide();
        } else {
            $('.pin').show();
        }
    })


})(jQuery);

